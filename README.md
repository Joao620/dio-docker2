# Como usar?
Primeiro rodar um `vagrant up` vai subir 3 nodes, node1 é o master.

Depois precisa rodar `vagrant ssh node1 -c 'sudo bash /vagrant/master_post.sh'` 
> Não encontrei uma maneira simples de criar o servico depois de todas vm's rodarem

E então, acessar qualquer endereço no localhost ente 8001-8003, cada um é uma vm, mas as requisições são balanceadas entre elas mesmo

Dá para verificar qual vm te respondeu pelo hostname dela, então só fica atualizando a página
> Tem um probleminha de cache, mas só desative ele no navegador que funciona direitinho
