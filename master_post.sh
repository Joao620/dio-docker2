docker service create \
  --name my-web \
  --publish published=8080,target=80 \
  --replicas 3 \
  --mount type=bind,src=./webboi/,dst=/usr/local/apache2/htdocs/ \
  httpd
