#!/bin/bash
#curl -fsSL https://get.docker.com | sudo bash
#sudo curl -fsSL "https://github.com/docker/compose/releases/download/1.25.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
#sudo chmod +x /usr/local/bin/docker-compose
echo "install_docker.sh"
cp /vagrant/docker/* /usr/bin/
sudo cp /vagrant/docker_systemctl/* /etc/systemd/system
sudo systemctl enable --now docker
groupadd docker
sudo usermod -aG docker vagrant
